package idr.core.validation;

public class InvoiceValidator implements Validator{

	@Override
	public boolean isAValidCuit(String cuit) {
		return CuitValidator.isValid(cuit);
	}
	
}
