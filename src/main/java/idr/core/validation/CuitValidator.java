package idr.core.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Precondicion: Recibe solo lo que sería el cuit esperado (sin la palabra cuit, ni :)
 * Precondicion: No se consideran cuits con guiones
 * @author Veronica
 *
 */
public class CuitValidator {
	private static String[] validTypes = {"20","23","24","25","26","27","30"};
	
	public static boolean isValid(String cuit){	
		//Se remueven los espacios en blancos que podrían producirse en una imagen.
		String aux = cuit.replaceAll("\\s+", "");
		if(hasElevenDigits(aux) && isValidType(aux)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Comprueba que hayan 11 digitos, juntos o con - 
	 * no acepta nada que tenga menos o más digitos.
	 * @param cuit
	 * @return
	 */
	private static boolean hasElevenDigits(String cuit) {		
		if(cuit.length()!=11)
			return false;
		Pattern elevenDigits = Pattern.compile("\\d{11}"); 
        Matcher matcher = elevenDigits.matcher(cuit);
        if(matcher.matches())
        	return true;
        else
        	return false;
	}
	
	private static boolean isValidType(String cuit) {
		for(String validType: validTypes) {
			if(cuit.startsWith(validType)) {
				return true;
			}
		}
		return false;
	}
}
