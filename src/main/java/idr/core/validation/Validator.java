package idr.core.validation;

public interface Validator {
	boolean isAValidCuit(String cuit);
}
