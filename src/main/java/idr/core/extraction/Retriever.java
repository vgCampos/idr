package idr.core.extraction;

import java.io.File;

/**
 * Interface del Recuperador de datos 
 * @author Verónica Campos
 *
 */
public interface Retriever {
	
	/**
	 * Extrae los datos hacia un invoice 
	 * @param file
	 */
	Invoice retrieveInvoiceData(File file);
	
	/**
	 * Permite que la implementación determine si puede
	 * manejar el archivo que le llega como parametro.
	 * @param file
	 */
	boolean isSuitable(File file);
	
}
