package idr.core.extraction;

import java.io.File;
import java.util.Set;

/**
 * Esta clase se encarga de seleccionar el retriever adecuado
 * @author Ver�nica Campos
 *
 */
public class RetrieverSelector{
	
	/**
	 * Al recibir la factura, el selector recorre la lista 
	 * de retrievers hasta que alguno sepa como interpretar 
	 * el invoice recibido.
	 * @param invoice 
	 * @return
	 */
	public static Retriever select(Set<Retriever> retrievers, File invoice) {
		for(Retriever ret: retrievers) {
			if(ret.isSuitable(invoice)) {
				return ret;
			}
		}
		return null;
	}
	
}
