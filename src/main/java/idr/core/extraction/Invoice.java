package idr.core.extraction;

public class Invoice {
	private String cuitSender;
	private String cuitReceiver;
	
	public Invoice() {
		this.cuitReceiver= "";
		this.cuitSender= "";
	}
	
	public String getCuitSender() {
		return cuitSender;
	}
	public void setCuitSender(String cuitSender) {
		this.cuitSender = cuitSender;
	}
	public String getCuitReceiver() {
		return cuitReceiver;
	}
	public void setCuitReceiver(String cuitReceiver) {
		this.cuitReceiver = cuitReceiver;
	}	
}
