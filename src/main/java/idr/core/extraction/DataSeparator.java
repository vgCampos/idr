package idr.core.extraction;

/**
 * Clase encargada de separar el dato del nombre del dato desde la linea recuperada.
 * @author Veronica Campos
 *
 */
public class DataSeparator {
	public static String extractCuit(String cuit){
		String extra = cuit.trim();
		extra = extra.replaceFirst("[c|C][u|U][i|I][t|T][:|]", "");
		extra = extra.trim();
		return extra;
	}
}
