package idr.core;

import java.beans.PropertyChangeListener;
import java.io.File;

public interface DataManager{
	void retrieveInvoiceData(File invoice);
	void addListener(PropertyChangeListener listener);
	void removeListener(PropertyChangeListener listener);
	void sendCuitSender();
	void sendCuitReceiver();
}
