package idr.core;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.Set;

import idr.core.extraction.Invoice;
import idr.core.extraction.Retriever;
import idr.core.extraction.RetrieverSelector;

public class InvoiceDataManager implements DataManager{
	
	private Set<Retriever> retrievers;
	private PropertyChangeSupport support;
	private Invoice invoice;
	
	public InvoiceDataManager(Set<Retriever> retrievers){
		support = new PropertyChangeSupport(this);
		this.retrievers = retrievers;
	}

	@Override
	public void retrieveInvoiceData(File file) {
		Retriever retriever = RetrieverSelector.select(retrievers, file);
		if(retriever==null) {
			support.firePropertyChange("supportFile", null, "Sin soporte, para: "+file.getName());
		}else {
			invoice = retriever.retrieveInvoiceData(file);
			if(invoice==null) {
				support.firePropertyChange("supportInvoice", null, "No se pudieron recuperar los datos de:"+file.getName());
			}else {
				support.firePropertyChange("invoiceReady", null, null);
			}
		}
	}

	@Override
	public void addListener(PropertyChangeListener listener) {
		support.addPropertyChangeListener(listener);
	}

	@Override
	public void removeListener(PropertyChangeListener listener) {
		support.removePropertyChangeListener(listener);
	}
	
	@Override
	public void sendCuitSender() {
		support.firePropertyChange("cuitSender", null, invoice.getCuitSender());
	}

	@Override
	public void sendCuitReceiver() {
		support.firePropertyChange("cuitReceiver", null, invoice.getCuitReceiver());
	}

}
