package idr.criterios_aceptacion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import idr.core.validation.InvoiceValidator;
import idr.core.validation.Validator;

public class US1_functional {
	private String cuit1;
	private String cuit2;
	private String cuit3;
	private String cuit4;
	private Validator validator;
	
	@Before
	public void setUpBeforeClass() throws Exception {
		cuit1 = "30123456789"; 		//Valido
		cuit2 = "301 23456789"; 	//Valido
		cuit3 = "1"; 				//Invalido
		cuit4 = "a0123456789";		//Invalido
		validator = new InvoiceValidator();
	}

	@Test
	public void test1() {
		Boolean isValid = validator.isAValidCuit(cuit1);
		assertTrue(isValid);
	}

	@Test
	public void test2() {
		Boolean isValid = validator.isAValidCuit(cuit2);
		assertTrue(isValid);
	}

	@Test
	public void test3() {
		Boolean isValid = validator.isAValidCuit(cuit3);
		assertFalse(isValid);
	}

	@Test
	public void test4() {
		Boolean isValid = validator.isAValidCuit(cuit4);
		assertFalse(isValid);
	}

}
