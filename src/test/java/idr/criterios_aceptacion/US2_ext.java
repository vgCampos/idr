package idr.criterios_aceptacion;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;

import idr.core.extraction.Retriever;
import idr.core.extraction.RetrieverSelector;

public class US2_ext {
	Set<Retriever> retrievers0;
	Set<Retriever> retrievers1;
	Set<Retriever> retrievers2;	

	@Mock 
	File file; 	
	@Mock 
	Retriever retrieverA;
	@Mock 
	Retriever retrieverB;
	@Mock 
	Retriever retrieverC;
	
	@Before
	public void load() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		when(retrieverA.isSuitable(file)).thenReturn(false);
		when(retrieverB.isSuitable(file)).thenReturn(false);
		when(retrieverC.isSuitable(file)).thenReturn(true);
		
		retrievers0 = new HashSet<Retriever>();
		
		retrievers1 = new HashSet<Retriever>();
		retrievers1.add(retrieverA);
		
		retrievers2 = new HashSet<Retriever>();
		retrievers2.add(retrieverB);
		retrievers2.add(retrieverC);
	}
	
	/**
	 * El selector de recuperadores de datos, no recibe ning�n recuperador,
	 * el comportamiento esperado es que devuelva null.
	 */
	@Test
	public void test0() {
		Retriever ret = RetrieverSelector.select(retrievers0, file);
		assertNull(ret);
	}
	/**
	 * El selector recibe una lista con un elemento recuperador, pero el mismo 
	 * no sabe como manejar el archivo, por tanto devuelve null
	 */
	@Test
	public void test1() {
		Retriever ret = RetrieverSelector.select(retrievers1, file);
		assertNull(ret);
	}

	/**
	 * El selector recibe dos recuperadores, el segundo retrieverC, 
	 * puede leer el archivo por tanto devuelve el retrieverC.
	 */
	@Test
	public void test2() {
		Retriever ret = RetrieverSelector.select(retrievers2, file);
		assertEquals(retrieverC, ret);
	}
}
