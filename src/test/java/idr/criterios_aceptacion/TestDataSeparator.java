package idr.criterios_aceptacion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import idr.core.extraction.DataSeparator;

public class TestDataSeparator {
	private String cuit1, cuit2,cuit3;
	@Before
	public void setUp() throws Exception {
		cuit1 = "CUIT:  30210216402"; 
		cuit2 = "Cuit: 12210216402"; 
		cuit3 = "cuit:272102"; 
	}

	@Test
	public void test1() {
		String cuit = DataSeparator.extractCuit(cuit1);
		assertEquals("30210216402",cuit);
	}

	@Test
	public void test2() {
		String cuit = DataSeparator.extractCuit(cuit2);
		assertEquals("12210216402",cuit);
	}

	@Test
	public void test3() {
		String cuit = DataSeparator.extractCuit(cuit3);
		assertEquals("272102",cuit);
	}

}
